package groupOfStudents;

import java.io.File;

public class Main {

	public static void main(String[] args) {
		GroupFileStorage storage = new GroupFileStorage();
		File academy = new File("D:/Google Drive/Java/Eclipse for Java/Project-Academy/");
		var add = new AddStudent();

		Student a1 = new Student("Oleksandr", "Lomachenko", Gender.MALE, 2520, "");
		Student a2 = new Student("Volodymyr", "Zelenskyi", Gender.MALE, 2530, "");
		Student a3 = new Student("Anna", "Gordiyenko", Gender.FEMALE, 2540, "");
		Student a4 = new Student("Olena", "Lomachenko", Gender.FEMALE, 2521, "");
		Student a5 = new Student("Vitaliy", "Klychko", Gender.MALE, 2531, "");
		Student a6 = new Student("Anastasiya", "Ivanova", Gender.FEMALE, 2541, "");
		Student a7 = new Student("Andriy", "Zadorojnyi", Gender.MALE, 2522, "");
		Student a8 = new Student("Mykhaylo", "Safronov", Gender.MALE, 2532, "");
		Student a9 = new Student("Oleksandr", "Lomachenko", Gender.MALE, 2520, "");

		Group javaOOP = new Group("Java OOP");
		var a10 = add.newStudent();

		try {
			javaOOP.addStudent(a1);
			javaOOP.addStudent(a2);
			javaOOP.addStudent(a3);
			javaOOP.addStudent(a4);
			javaOOP.addStudent(a5);
			javaOOP.addStudent(a6);
			javaOOP.addStudent(a7);
			javaOOP.addStudent(a8);
			javaOOP.addStudent(a9);
			javaOOP.addStudent(a10);
		} catch (GroupOverflowException e) {
			e.printStackTrace();
		}

		System.out.println(javaOOP);

		storage.saveGroupToCSV(javaOOP, academy);

		File importGroup = storage.findFileByGroupName("Java Start", academy);

		Group newGroup = storage.loadGroupFromCSV(importGroup);

		System.out.println(newGroup);

		System.out.println(javaOOP.getGroupName() + " hash: " + javaOOP.hashCode());
		System.out.println(newGroup.getGroupName() + " hash: " + newGroup.hashCode());
		System.out.println();

		javaOOP.hasEquivalentStudents();
		newGroup.hasEquivalentStudents();

		try {
			System.out.println();
			System.out.println(javaOOP.searchStudentByLastName("Safron"));
		} catch (StudentNotFoundException e) {
			e.printStackTrace();
		}

		System.out.println();
		javaOOP.removeStudentById(2520);
		System.out.println(javaOOP);

	}

}
