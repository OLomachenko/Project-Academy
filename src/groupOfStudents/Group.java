package groupOfStudents;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class Group {
	private String groupName;
	private final List<Student> students = new ArrayList<>();

	public Group(String groupName) {
		super();
		this.groupName = groupName;
	}

	public Group() {
		super();
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public List<Student> getStudents() {
		return students;
	}

	public void addStudent(Student student) throws GroupOverflowException {
		if (students.size() < 10) {
			students.add(student);
			student.setGroupName(this.groupName);
			return;
		}

		throw new GroupOverflowException("Group overflow");
	}

	public Student searchStudentByLastName(String lastName) throws StudentNotFoundException {
		for (Student st : students) {
			if (st.getLastName().contains(lastName)) {
				return st;
			}
		}
		throw new StudentNotFoundException("Student doesn't exist");
	}

	public boolean removeStudentById(int id) {
		for (Student st : students) {
			if (st.getId() == id) {
				students.remove(st);
				return true;
			}
		}
		return false;

	}

	public void hasEquivalentStudents() {
		boolean result = false;
		for (int i = 0; i < students.size() - 1; i++) {
			for (int j = i + 1; j < students.size(); j++) {
				if (students.get(i).equals(students.get(j))) {
					result = true;
					break;
				}
			}
		}
		if (result) {
			System.out.println("Group " + groupName + " has duplicated entries of type Student");
		} else {
			System.out.println("Group " + groupName + " doesn't have duplicated students");
		}
	}

	public void sortStudentsByLastName() {
		Collections.sort(students, Comparator.nullsLast(new StudentLastNameComparator<Student>()));

	}

	@Override
	public int hashCode() {
		return Objects.hash(groupName, students);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group other = (Group) obj;
		return Objects.equals(groupName, other.groupName) && Objects.equals(students, other.students);
	}

	@Override
	public String toString() {
		sortStudentsByLastName();
		String group = "Group " + groupName + ":" + System.lineSeparator();
		for (Student st : students) {
			group += st + System.lineSeparator();

		}

		return group;
	}

}