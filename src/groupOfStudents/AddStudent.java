package groupOfStudents;

import java.util.Scanner;

public class AddStudent {
	Scanner sc = new Scanner(System.in);

	public Student newStudent() {
		System.out.println("Enter new Student's parameters:");
		System.out.println("\"Name\", \"Last Name\", \"Gender\"(MALE/FEMALE), \"ID\"");

		try {
			String[] stud = sc.nextLine().split(",\\s");
			Student student = new Student();

			student.setName(stud[0]);
			student.setLastName(stud[1]);
			student.setGender(Gender.valueOf(stud[2]));
			student.setId(Integer.valueOf(stud[3]));
			return student;
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Wrong input!");
			return null;
		}

	}
}
