package groupOfStudents;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Scanner;

public class GroupFileStorage {

	public void saveGroupToCSV(Group gr, File folder) {
		File group = new File(folder + gr.getGroupName() + ".csv");
		List<Student> students = gr.getStudents();
		String result = "";

		try (Writer pw = new FileWriter(group)) {
			for (Student st : students) {
				result += StringConverter.toStringRepresentation(st) + System.lineSeparator();
			}
			pw.write(result);
			System.out.println("Group: " + gr.getGroupName() + " successfully saved to csv file\n");
		} catch (IOException e) {
			System.out.println("Some problem with file to write");
		}

	}

	public Group loadGroupFromCSV(File file) {
		Group st = new Group(file.getName().substring(0, file.getName().indexOf(".")));
		try (Scanner sc = new Scanner(file)) {
			while (sc.hasNextLine()) {
				st.addStudent(StringConverter.fromStringRepresentation(sc.nextLine()));
			}
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		} catch (GroupOverflowException e) {
			e.toString();
		}
		return st;
	}

	public File findFileByGroupName(String groupName, File workFolder) {
		if (workFolder.isDirectory()) {
			File[] files = workFolder.listFiles();

			for (int i = 0; i < files.length; i++) {
				if (files[i].getName().contains(groupName)) {
					return files[i].getAbsoluteFile();
				}
			}
		}
		return null;

	}
}
